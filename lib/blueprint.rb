require 'compass'


base_directory  = File.join(File.dirname(__FILE__), '..')
stylesheets_dir = File.join(base_directory, 'stylesheets')
Compass::Frameworks.register('blueprint', :stylesheets_directory => stylesheets_dir)

module Blueprint
  VERSION = "1.0.1"
  DATE = "2014-10-13"
end
